<?php
require_once ('init.php');
//on récup les as et tache avec get car c'est de l'url
//on verifie si as est present part isset
if(isset($_GET['as']) && isset($_GET['tache'])){
  //on stock les as et tache dans des variables
  $as =$_GET['as'];
  $tache = $_GET['tache'];
// j'utilise switch pour verifier plusieurs conditions
  switch($as){
    case 'done':
// on utilise le as=done envoyé dans le lien et on le met dans la condition si mon as est egale à done alore je modifie ma colonne dans le bdd
    $donerq = $cnx -> prepare("UPDATE liste
       SET done = 1
       WHERE id = :tache
       AND users = :users");
//je dis si as est = à done tu modifies le donne et tu lui donne la valeur de 1 et tu recup le id de la tache et le id du user
    $donerq->execute([
      'tache'=>$tache,
      'users'=>$_SESSION['id']
      ]);

    break;
    // test mais la suite n'est pas utile
    case 'nodone':
    $donerq = $cnx -> prepare("UPDATE liste
      SET done = 0
     WHERE id= :tache
     AND users = :users"
   );
    $donerq -> execute([
      'tache'=>$tache,
      'users'=>$_SESSION['id']
      ]);
    break;
  }
}
// une fois mes conditions et mes changements effect retourne à la page index
header('LOCATION:index.php');
